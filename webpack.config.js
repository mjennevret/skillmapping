var webpack = require('webpack');

module.exports = {
    entry: "./skillset/javascript/entry.js",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
              test: /\.jsx?$/,
              exclude: /node_modules/,
              loader: "babel-loader",
              query: {
                presets: ['es2015','react']
              }
            }
        ]
    }
};
