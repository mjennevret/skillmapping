<?php

class CompetencyCollection extends DataObject {
	
	private static $db = array(
		'Name' => 'Varchar(45)',
		'Description' => 'Text',
        'AutoInvite' => 'Boolean'
	);
	
    private static $defaults = array(
        'AutoInvite' => false
    );
    
    private static $has_one = array(
        'Owner' => 'Member'
    );
    
	private static $has_many = array(
		'CompetencyAreas' => 'CompetencyArea',
        'Invitations' => 'Invitation'
	);
	
	private static $default_sort = array(
		'Name'
	);
		
	private static $summary_fields = array(
		'Name'
	);
    
	public function getCMSFields () {
		
		$fields = parent::getCMSFields();

        // On an empty Collection there is no CompetencyAreas field
        if($fields->dataFieldByName('CompetencyAreas')) {
            $fields->dataFieldByName('CompetencyAreas')->getConfig()->removeComponentsByType('GridFieldAddExistingAutocompleter');
        }

        if($fields->dataFieldByName('Invitations')) {
            $fields->dataFieldByName('Invitations')->getConfig()->removeComponentsByType('GridFieldAddExistingAutocompleter');
        }
        
        // Automatically set Owner to the logged in Member. Unless Admin.
		if(!Permission::checkMember(Member::currentUser(), 'ADMIN')) {
			$fields->replaceField('OwnerID', new HiddenField('OwnerID', 'Owner', Member::currentUserID()));
            $fields->removeByName('AutoInvite');
		}

		return $fields;
	}
    
	/**
	 * Clean up relationships before deleting
	 * 
	 * @return void
	 */
	public function onBeforeDelete () {
		
		parent::onBeforeDelete();
		$this->getComponents('CompetencyAreas')->removeAll();
	}
	
    /**
     * @todo update all invited members skillsets if a competency might have been added.
     */
    public function onAfterWrite() {
        parent::onAfterWrite();
       
    }
    
	/**
	 * Validation performed before writing record to DB
	 * 
	 * @return ValidationResult
	 */
	public function validate() {
		
		$result = parent::validate();
		
		if(empty($this->Name)) {
			$result->error('Name may not be empty');
		}
		
		return $result;
	}
    
    public function canView($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }
}
