<?php

class CompetencyArea extends DataObject {
	
	private static $db = array(
		'Name' => 'Varchar(45)',
		'Description' => 'Text'
	);
	
	private static $has_one = array(
		'CompetencyCollection' => 'CompetencyCollection'
	);
	
	private static $has_many = array(
		'Competencies' => 'Competency.CompetencyArea'
	);
	
	private static $default_sort = array(
		'Name'
	);
		
	private static $summary_fields = array(
		'Name'
	);
	
	//private static $searchable_fields = array();
	
	public function getFullTitle() {
        return $this->Name;
		//return $this->CompetencyCollection()->Name.' - '.$this->Name;
	}
	
   	public function getCMSFields () {	
		$fields = parent::getCMSFields();
        if($fields->dataFieldByName('Competencies')) {
            $fields->dataFieldByName('Competencies')->getConfig()->removeComponentsByType('GridFieldAddExistingAutocompleter');
        }
		return $fields;
	}

    
	/**
	 * Clean up relationships before deleting
	 * 
	 * @return void
	 */
	public function onBeforeDelete () {
		
		parent::onBeforeDelete();
		$this->getComponents('Competencies')->removeAll();
	}
	
	/**
	 * Validation performed before writing record to DB
	 * 
	 * @return ValidationResult
	 */
	public function validate() {
		
		$result = parent::validate();

		if(empty($this->CompetencyCollectionID)) {
			$result->error('CompetencyCollection may not be empty');
		}
		
		if(empty($this->Name)) {
			$result->error('Name may not be empty');
		}
		
		// Check for duplicate
		$rec = DataObject::get('CompetencyArea')->where(array('Name' => $this->Name, 'CompetencyCollectionID' => $this->CompetencyCollectionID))->first();
		if(!empty($rec) && $rec->ID != $this->ID) {
			$result->error('This record already exists');
		}
		
		return $result;
	}
    
    public function canView($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }
}