<?php

class Invitation extends DataObject {
	
	private static $db = array(
		'Email' => 'Varchar(45)',
        'Token' => 'Varchar(45'
	);
	
	private static $has_one = array(
		'CompetencyCollection' => 'CompetencyCollection',
        'Invitee' => 'Member'
	);
		
	private static $summary_fields = array(
		'Email'
	);
	
    public function getTitle() {
        return $this->CompetencyCollection()->Name . ' -> ' . $this->Email;
    }
    
	public function getCMSFields () {
		
		$fields = parent::getCMSFields();
    	$fields->removeByName('InviteeID');
        $fields->removeByName('Token');
		return $fields;
	}
	
    public function onBeforeWrite() {
        parent::onBeforeWrite();
        if(empty($this->Token)) {
            $this->Token = md5($this->ID);
            $this->inviteMember($this->Email); // To only send once
        }
    }
    
	/**
	 * Validation performed before writing record to DB
	 * 
	 * @return ValidationResult
	 */
	public function validate() {
		
		$result = parent::validate();

		if(empty($this->Email)) {
			$result->error('Email may not be empty');
		}
			
		// Check for duplicate
		$rec = DataObject::get('Invitation')->where(array("Email = '$this->Email'", "CompetencyCollectionID = '$this->CompetencyCollectionID'"))->first();
		if(!empty($rec) && $rec->ID != $this->ID) {
			$result->error($this->Email .' has already recieved an invitation for this collection');
		}
		
		return $result;
	}
    
	public function canDelete($member = null) {
		
		if(!isset($member)) {
			$member = Member::currentUser();
		}
		return $member->ID == $this->CompetencyCollection()->OwnerID;
	}
    
    /**
     * Invite action.
     * Invite member to fill the skill cards.
     * New member shall be created if not already existing.
     * New set of empty skill cards shall be generated.
     */
    public function inviteMember($email) {
        // Create member
        $member = $this->createMember($email);
        $this->InviteeID = $member->ID;
        // Create skill cards
        $areas = $this->CompetencyCollection()->getComponents('CompetencyAreas');
        foreach($areas as $competencyArea) {
            $competencies = $competencyArea->getComponents('Competencies');
            foreach($competencies as $competency) {
                $this->createSkillCard($member->ID, $competency->ID);
            }
        }
        // Do not send out emails in Auto invites. ie. demo collections.
        if(!$this->CompetencyCollection()->AutoInvite) {
            $this->sendInvitationEmail($this->CompetencyCollection()->Owner(), $member);
        }
    }
    
    /**
     * The link that will automatically log the user in and place them directly
     * on the first skill card.
     * 
     * @return string
     */
    public function getEntryLink() {
        return "/EntryController/index/$this->Token";
    }
    
    protected function sendInvitationEmail($inviter, $invitee) {
        $email = new Email();
        $email
            ->setFrom($inviter->Email)
            ->setTo($invitee->Email)
            ->setSubject("SkillMapping invitation from $inviter->FullName")
            ->setTemplate('InvitationEmail')
            ->populateTemplate(new ArrayData(array(
                'Inviter' => $inviter,
                'Invitee' => $invitee,
                'EntryLink' => $this->getEntryLink()
        )));

        $email->send();
    }

    protected function createSkillCard($memberID, $competencyID) {
        $skill = Skill::get_one('Skill', "ProfessionalID = $memberID AND CompetencyID = $competencyID");
        if(!empty($skill)) {
            return $skill;
        }
        
        $skillCard = new Skill();
        $skillCard->ProfessionalID = $memberID;
        $skillCard->CompetencyID = $competencyID;
        $skillCard->write();
        return $skillCard;
    }
    
    protected function createMember($email) {
		$email = trim($email);
		$member = Member::get_one('Member', 'Email = \''.$email.'\'');
		if(!empty($member)) {
			return $member;
		}
        
		$validEmailFormat = filter_var($email, FILTER_VALIDATE_EMAIL);
		if(!$validEmailFormat) {
			throw new Exception('Email invalid', 2);
		}
		
		$newMember = new Member();
		$newMember->FirstName = 'Auto';
		$newMember->Surname = 'Generated';
		$newMember->Email = $email;
		$newMember->write();
        // SkillfulMemberExtension assigns the new member to skillset-member group onAfterWrite
		return $newMember;
	}
    
    public function canView($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }
}