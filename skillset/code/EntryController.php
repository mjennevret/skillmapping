<?php

/**
 *
 * @author morgan
 */
class EntryController extends Controller {

	/**
     * Automatically login user invited by the given token.
     * Redirect straight to the first skill card.
     * 
	 * @return void
	 */
	public function index() {
        
        if(empty($invitationToken = $this->request->param('ID'))) {
            $this->getRequest()->setStatusCode(404);
            return $this->getRequest();
        }
        
        // Let the ORM handle sanity and escaping of the passed token.
        if(empty($invitation = DataObject::get_one('Invitation', "Token='$invitationToken'"))) {
            $this->getRequest()->setStatusCode(404);
            return $this->getRequest();
        }
		
		if(Member::currentUser()) {
			//Member::currentUser()->logOut();
		}
        //Debug::dump(Member::currentUserID());
		//Debug::dump($invitation->Invitee()); die;
		//if(Member::currentUserID() != $invitation->inviteeID) {
			$invitation->Invitee()->logIn();
		//}
		
		$this->redirect('/profile/skillset/'.$invitation->CompetencyCollectionID);
	}
}
