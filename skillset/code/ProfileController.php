<?php

/**
 *
 * @author morgan
 */
class ProfileController extends Controller {

	static $allowed_actions = array(
		'view', 'skillset', 'skill', 'getcompetencycollections', 'saveskillcard', 'notifyowner'
	);	
	
	protected $member;
    protected $presetCompetencyCollectionID;
	
	/**
	 * Default action
	 * @return string
	 */
	public function view() {
		
		return $this->renderWith('ProfilePage');
	}
	
    /**
     * CompetencyCollectionHolder
     * @return string
     */
	public function skillset() {
		
        if(!empty($collectionID = $this->request->param('ID'))) {
            $this->presetCompetencyCollectionID = (int) $collectionID;
        }
        return $this->skill();
	}
	
    /**
     * Skill card
     * @return string
     */
	public function skill() {
		
		if(!$this->getMember()) {
			$this->redirect('/');
			return;
		}
        
		return $this->renderWith('SkillEdit');
	}
		
    public function getPresetCompetencyCollectionID() {
        return $this->presetCompetencyCollectionID ? $this->presetCompetencyCollectionID : 0;
    }
    
	/**
	 * Logged in Member
	 * @return Member
	 */
	public function getMember() {
		
		if(empty($this->member)) {
			$this->member = Member::currentUser();
		}
		
		return $this->member;
	}
    
    /**
     * The Member whos profile you are viewing
     * @todo Optimize by caching the response. This method is called upon a lot.
     * @return Member
     */
    public function getProfileMember() {
        
		$memberID = (int) $this->request->param('ID');
		if(!$this->validateMemberID($memberID)) {
			return $this->httpError('404', 'Member identified by "'.$memberID.'" does not exist');
		}
		return Member::get_by_id('Member', $memberID);
    }
	
	/**
	 * 
	 * @param int $id
	 * @return boollean
	 */
	private function validateMemberID($id){

		$member = DataObject::get_by_id('Member', $id);
		return FALSE !== $member ;
	}
    
    /**
     * Get the collections for the profile member.
     * @return array
     */
    public function getCompetencyCollectionsForProfile() {
        
        $profileMember = $this->getProfileMember();
        $array = $this->CompetencyCollections($profileMember->ID);
        foreach($array as $i => $collection) {
            $array[$i]['LinkAreas'] = $profileMember->getLinkReportCompetencyAreas($collection['ID']);
            $array[$i]['LinkSkills'] = $profileMember->getLinkReportSkills($collection['ID']);
        }
        $arrayList = new ArrayList($array);
        return $arrayList;
    }
    
    /**
     * 
     * @param int $memberID
     * @return array
     */
    private function CompetencyCollections($memberID = NULL) {
        $sql = "SELECT
            cc.ID as CompetencyCollectionID,
            cc.Name AS CompetencyCollectionName,
            cc.OwnerID AS OwnerID,
            s.ID AS SkillID,
            c.ID AS CompetencyID,
            c.Name AS CompetencyName,
            s.Interest AS Interest,
            s.Proficiency AS Proficiency,
            s.ProfessionalID AS ProfessionalID
            FROM CompetencyCollection cc
            JOIN CompetencyArea ca ON cc.ID = ca.CompetencyCollectionID
            JOIN Competency c ON ca.ID = c.CompetencyAreaID
            JOIN Skill s ON c.ID = s.CompetencyID
            WHERE s.ProfessionalID = $memberID";        
            
        $result = DB::query($sql);
        $resultArr = array();
        while($row = $result->nextRecord()) {
            if(empty($resultArr[$row['CompetencyCollectionID']])) {
                $resultArr[$row['CompetencyCollectionID']] = array(
                    'ID' => $row['CompetencyCollectionID'],
                    'name' => $row['CompetencyCollectionName'],
                    'owner' => $row['OwnerID'],
                    'professional' => $row['ProfessionalID'],
                    'skills' => array()
                );
            }
            $resultArr[$row['CompetencyCollectionID']]['skills'][] = array(
                'ID' => $row['SkillID'],
                'Proficiency' => $row['Proficiency'],
                'Interest' => $row['Interest'],
                'CompetencyID' => $row['CompetencyID'],
                'CompetencyName' => $row['CompetencyName'],
                'ProfessionalID' => $row['ProfessionalID']
            );
        }
        return $resultArr;
    }
    
    /*************************
     * Ajax actions
     */
    
    /**
     * 
     * @return SS_HTTPResponse
     */
    public function getcompetencycollections() {
        
        if(empty($memberID = $this->getMember()->ID)) {
            $this->getResponse()->setStatusCode(401);
            return $this->getResponse();
        }
        
        $collections = $this->CompetencyCollections($memberID);
        $this->getResponse()->addHeader("Content-type", "application/json");
        $this->getResponse()->setBody(json_encode(array_values($collections)));
        return $this->getResponse();
    }
    
    /**
     * 
     * @return SS_HTTPResponse
     */
    public function saveskillcard() {
        
        if(empty($memberID = $this->getMember()->ID)) {
            $this->getResponse()->setStatusCode(401);
            return $this->getResponse();
        }
        
        parse_str($this->request->getBody(), $newSkillCard);
        $skillObj = Skill::get_by_id('Skill', $newSkillCard['ID']);
        if(empty($skillObj)) {
            $this->getResponse()->setStatusCode(500);
            $this->getResponse()->setBody('Skill id ' . $newSkillCard['ID'] . ' was not found in database can can not be updated.');
            return $this->getResponse();
        }
        
        $skillObj->Interest = $newSkillCard['Interest'];
        $skillObj->Proficiency = $newSkillCard['Proficiency'];
        $skillObj->write();
        
        $this->getResponse()->addHeader("Content-type", "application/json");
        $this->getResponse()->setBody(json_encode($newSkillCard));
        return $this->getResponse();
    }
    
    /**
     * Send an email notification to the owner of the competency collection
     * that an invited member has completed all the skill cards.
     * 
     * @return SS_HTTPResponse
     */
    public function notifyowner() {
        
        if(empty($memberID = $this->getMember()->ID)) {
            $this->getResponse()->setStatusCode(401);
            return $this->getResponse();
        } 
        
        if(empty($competencyCollectionID = $this->request->param('ID'))) {
            $this->getResponse()->setStatusCode(400);
            return $this->getResponse();
        }
        
        if(empty($competencyCollection = DataObject::get_by_id('CompetencyCollection', $competencyCollectionID))) {
            $this->getResponse()->setStatusCode(400);
            return $this->getResponse();
        }
        
        // Do not send notification email if collection is AutoInvite. Ie. demo collection.
        if($competencyCollection->AutoInvite) {
            $this->getResponse()->setBody('No notifications sent on Demo collection');
            return $this->getResponse();
        }
        
        $invitee = $this->getMember();
        $email = new Email();
        $email
            ->setFrom($competencyCollection->Owner()->Email)
            ->setTo($competencyCollection->Owner()->Email)
            ->setSubject("$competencyCollection->Name skill cards compteted by $invitee->FullName")
            ->setTemplate('NotificationEmail')
            ->populateTemplate(new ArrayData(array(
                'CompetencyCollection' => $competencyCollection,
                'Invitee' => $invitee
        )));
        $email->send();
        
        return $this->getResponse();
    }
}
