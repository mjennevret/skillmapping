<?php

class CompetencyAdmin extends ModelAdmin {

    private static $managed_models = array(
        'CompetencyCollection'
    );

    private static $url_segment = 'competency';

    private static $menu_title = 'Competency';
	
	public $showImportForm = FALSE;
    
    /**
     * Only let user see their own collections
     * 
     * @return DataList
     */
    public function getList() {
        $list = parent::getList();
        if($this->modelClass == 'CompetencyCollection') {
            $list = $list->filter('OwnerID', Member::currentUserID());
        }
        return $list;
    }
}

