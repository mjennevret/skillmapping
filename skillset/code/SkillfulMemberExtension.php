<?php

class SkillfulMemberExtension extends DataExtension {
    
    public function getFullName() {
        return $this->owner->FirstName . ' ' . $this->owner->Surname;
    }
	
    /**
     * Profile page
     * @return string
     */
	public function Link() {
		
		return '/profile/view/'.$this->owner->ID;
	}
	
    /**
     * 
     * @return string
     */
	public function getLinkReportCompetencyAreas($competencyCollectionID) {
		
		return '/report/areas/'.$this->owner->ID.'/'.$competencyCollectionID;
	}

	/**
	 *  
	 * @return string
	 */
	public function getLinkReportSkills($competencyCollectionID) {

		return '/report/skills/'.$this->owner->ID.'/'.$competencyCollectionID;
	}
	
    /**
     * CompetencyCollectionsHolder
     * @return string
     */
	public function LinkEditSkillset() {
		
		return '/profile/skillset';
	}
    
    /**
     * This extensions makes it impossible to remove a member from group skillset-member.
     * Should be done in a better way but for now it does the job.
     *
     * I have not figured out a pretty way to only execute this on the initial 
     * create. Since "onAfterWrite" the ID will always exist.
     * 
     * @return void
     */
    public function onAfterWrite() {
        
        if(!$this->owner->inGroup('skillset-member')) {
            $this->owner->addToGroupByCode('skillset-member');
        }
        
        // Generate Auto invitations
        $collections = DataObject::get('CompetencyCollection')->where('AutoInvite=1');
        foreach($collections as $collection) {
            $invitationExists = DataObject::get_one(
                'Invitation', 
                "InviteeID={$this->owner->ID} AND CompetencyCollectionID={$collection->ID}"
            );
            if($invitationExists) {
                continue;
            }
            $invitation = new Invitation();
            $invitation->Email = $this->owner->Email;
            $invitation->CompetencyCollectionID = $collection->ID;
            $invitation->InviteeID = $this->owner->ID;
            $invitation->write();
        }
    }
}