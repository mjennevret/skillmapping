<?php

/**
 *
 * @author morgan
 */
class ReportController extends Controller {
	
	static $allowed_actions = array(
        'skills', 'areas'
	);
	
    static $url_handlers = array(
        'skills/$MemberID/$CollectionID/$AreaID' => 'skills',
        'areas/$MemberID/$CollectionID' => 'areas'
    );
    
	public function skills() {
        
		if(!($member = $this->getMember())) {
			return $this->httpError(404, 'Member identified by "'.$this->getMemberID().'" does not exist');
		}

		if(!($collection = $this->getCollection())) {
			return $this->httpError(404, 'Competency collection identified by "'.$this->getCollectionID().'" does not exist');
		}
		
		return $this->renderWith('SkillReport');		
	}
	
	public function areas() {

		if(!($member = $this->getMember())) {
			return $this->httpError(404, 'Member identified by "'.$this->getMemberID().'" does not exist');
		}

		if(!($collection = $this->getCollection())) {
			return $this->httpError(404, 'Competency collection identified by "'.$this->getCollectionID().'" does not exist');
		}
        
		return $this->renderWith('AreasReport');
	}
	
    protected function getMemberID() {
        return (int) $this->request->param('MemberID');
    }
    
	public function getMember() {
		$member = Member::get_by_id('Member', $this->getMemberID());
        return ($member) ? $member : FALSE;
	}
    
    protected function getCollectionID() {
        return (int) $this->request->param('CollectionID');
    }    
    
    public function getCollection() {        
        $collection = DataObject::get_by_id('CompetencyCollection', $this->getCollectionID());
        return ($collection) ? $collection : FALSE;
    }
    
    protected function getAreaID() {
        return (int) $this->request->param('AreaID');
    }
    
    public function getArea() {
        $area = DataObject::get_by_id('CompetencyArea', $this->getAreaID());
        return ($area) ? $area : FALSE;
    }
	
    /**
     * 
     * @return string
     */
	public function getJsonSkillsData() {
		
		$skillset = $this->getSkillset();
		$result = array();
		foreach($skillset as $skill) {
			$result[] = array(
				'Name' => $skill->Competency()->Name,
				'Proficiency'.$skill->Competency()->CompetencyAreaID => $skill->Proficiency,
				'Interest'.$skill->Competency()->CompetencyAreaID => $skill->Interest,
				'Weight' => $skill->Competency()->Weight
			);
		}

		return json_encode($result);		
	}
	
    /**
     * 
     * @return string
     */
	public function getJsonAreasData() {
		
		$skillset = $this->getSkillset();
		$result = array();
		$stack = array();
		foreach($skillset as $skill) {
			$area = $skill->Competency()->CompetencyArea();
			if(empty($result[$area->ID])) {
				$result[$area->ID] = array(
					'Name' => $area->Name,
					'Proficiency'.$area->ID => 0,
					'Interest'.$area->ID => 0,
					'SkillCount' => 0
				);
				$stack[$area->ID] = array(
					'Proficiency' => 0,
					'Interest' => 0
				);
			}
			
			$stack[$area->ID]['Proficiency'] += $skill->Proficiency;
			$stack[$area->ID]['Interest'] += $skill->Interest;
			$result[$area->ID]['SkillCount']++;
			$result[$area->ID]['Proficiency'.$area->ID] = round($stack[$area->ID]['Proficiency']/$result[$area->ID]['SkillCount'], 2);
			$result[$area->ID]['Interest'.$area->ID]    = round($stack[$area->ID]['Interest']/$result[$area->ID]['SkillCount'], 2);
			$result[$area->ID]['URL'] = $this->getLinkSubSkillset($area->ID);
		}
		
		return json_encode(array_values($result));
	}
	
	/**
	 * 
	 * @return DataList
	 */
	public function getGraphMetaData() {
		
		$ownerID = empty($this->managerID) ? 0 : $this->managerID;
		$skillset = $this->getSkillset($ownerID);
		if(0 == $skillset->count()) {
			$dl = new DataList('CompetencyArea');
			return $dl->where('1=2');
		}
		
		$areaset = array();
		foreach($skillset as $skill) {
			$areaset[$skill->Competency()->CompetencyAreaID] = $skill->Competency()->CompetencyAreaID;
		}
		
		$filter = 'ID IN('.implode(',', $areaset).')';
		$competencyAreas = CompetencyArea::get()->where($filter);
		return $competencyAreas;
	}
	
	public function getLinkProfile() {
		
		return '/profile/view/'.$this->getMemberID();
	}
	
    public function getLinkAreas() {
        return '/report/areas/'.$this->getMemberID().'/'.$this->getCollectionID();
    }
    
	protected function getLinkSubSkillset($areaId) {
		
		return '/report/skills/'.$this->getMemberID().'/'.$this->getCollectionID().'/'.$areaId;
	}
    
	public function getSkillset() {
		
        $filters = array(
            'ProfessionalID = '.$this->getMemberID(), 
            'Proficiency > 0', 
            'Interest > 0', 
            'CompetencyArea.CompetencyCollectionID = '.$this->getCollectionID()
        );
        
        if(!empty($this->getAreaID())) {
            $filters[] = 'Competency.CompetencyAreaID = '.$this->getAreaID();
        }
        
        $skillset = Skill::get()
                ->innerJoin('Competency', 'Skill.CompetencyID = Competency.ID')
                ->innerJoin('CompetencyArea', 'Competency.CompetencyAreaID = CompetencyArea.ID')
                ->where($filters);
		
		return $skillset; 
	}
}
