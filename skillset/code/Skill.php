<?php

/**
 * Description of Skill
 *
 * @author morgan
 */
class Skill extends DataObject {
	
	private static $db = array(
		'Proficiency' => 'Int', 
		'Interest' => 'Int',
		//'Comment' => 'Text'
	);
	
	private static $has_one = array(
		'Competency' => 'Competency',
		'Professional' => 'Member'
	);
	
	private static $summary_fields = array('Professional.Title', 'Competency.Name');
	
	protected function validate() {
	
		$result = parent::validate();
		
		if($this->Proficiency < 0 || $this->Proficiency > 4) {
			$result->error('Proficiency has to be rated between 0-4');
		}
		
		if($this->Interest < 0 || $this->Interest > 4) {
			$result->error('Interest has to be rated between 0-4');
		}
		
		return $result;
	}
}
