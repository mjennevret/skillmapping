<?php

class Competency extends DataObject {
	
	private static $db = array(
		'Name' => 'Varchar(45)',
		'Weight' => 'Int()',
		'Description' => 'Text'
	);
	
	private static $has_one = array(
		'CompetencyArea' => 'CompetencyArea'
	);
	
	private static $has_many = array(
		'Skills' => 'Skill'
	);
	
	private static $default_sort = array(
		'Name'
	);
		
	private static $summary_fields = array(
		'Name'
	);
	
	private static $searchable_fields = array();
	
	public function getCMSFields () {
		
		$fields = parent::getCMSFields();
        
		$fields->dataFieldByName('CompetencyAreaID')->getSource()->setValueField('FullTitle');
			
		if(!Permission::checkMember(Member::currentUser(), 'ADMIN')) {
			$fields->replaceField('Weight', new HiddenField('Weight', 'Weight', 1));
			$fields->replaceField('Description', new HiddenField('Description'));
			$fields->removeByName('Skills');
		}

		return $fields;
	}

    public function getOwner() {
        return $this->CompetencyArea()->CompetencyCollection()->Owner();
    }
    
	/**
	 * Clean up relationships before deleting
	 * 
	 * @return void
	 */
	public function onBeforeDelete () {
		
		parent::onBeforeDelete();
		$this->getComponents('Skills')->removeAll();
	}
	
	/**
	 * Validation performed before writing record to DB
	 * 
	 * @return ValidationResult
	 */
	public function validate() {
		
		$result = parent::validate();

		if(empty($this->CompetencyAreaID)) {
			$result->error('Area may not be empty');
		}
		
		if(empty($this->Name)) {
			$result->error('Name may not be empty');
		}
		
		if($this->Weight < 1 || $this->Weight > 4) {
			$result->error('Weight must be a value between 1-4');
		}
		
		// If another record exists with the same Name Area combination
		$rec = DataObject::get('Competency')->where(array("Name = '$this->Name'", "CompetencyAreaID = '$this->CompetencyAreaID'"))->first();
		if(!empty($rec) && $rec->ID != $this->ID) {
			$result->error('This record already exists');
		}
		
		return $result;
	}
	
    public function canView($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_CompetencyAdmin', 'any', $member);
    }    
    
	public function canDelete ($member = null) {
		
		if(!isset($member)) {
			$member = Member::currentUser();
		}
		return $member->ID == $this->getOwner()->ID;
	}
}