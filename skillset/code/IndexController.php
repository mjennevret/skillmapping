<?php

/**
 *
 * @author morgan
 */
class IndexController extends Controller {

	static $allowed_actions = array('LoginForm', 'RegistrationForm');
	
	/**
	 * Default action
	 * @return string
	 */
	public function index() {
		
		if('/why' == $this->requestParams['url']) { // Dirty hack out of laziness
			return $this->renderWith('Why');
		}
		
		Session::set('BackURL', '/profile/skillset');
		return $this->renderWith('Index');
	}
	
	public function LoginForm() {
		
        $form = new MemberLoginForm($this, __FUNCTION__);
        foreach($form->VisibleFields() as $field) {
            if(in_array($field->getAttribute('type'), array('text', 'password'))) {
                $field->setAttribute('placeholder', $field->Name);
                $field->setTitle(null);
            } elseif($field->getAttribute('name') === 'Remember') {
                $field->setTitle('Remember');
            }
        }
		return $form;
	}

	public function RegistrationForm() {
		
        $form = new MemberRegistrationForm($this, __FUNCTION__);
        foreach($form->Fields() as $field) {
            if($field instanceof ConfirmedPasswordField) {
                foreach($field->getChildren() as $pwField) {
                    $pwField->setAttribute('placeholder', $pwField->Title());
                    $pwField->setTitle(null);
                }
            }
            elseif(in_array($field->getAttribute('type'), array('text', 'password', 'email'))) {
                $field->setAttribute('placeholder', $field->Name);
                $field->setTitle(null);
            }
        }
		return $form;
	}
	
}
