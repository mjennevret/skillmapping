import React from 'react'
import * as api from './api-utils.js'

export default class SkillEdit extends React.Component {

  constructor() {
    super();
    this.state = {
      currentSkill: null
    }
  }

  componentWillMount() {
    this.setState({currentSkill: this.props.firstSkillCard})
  }

  render() {
    if (!this.state.currentSkill) {
        this.renderSpinner();
    }

    return (
      <div className="skilledit-wrapper">
        <div className="skill-form">
          {this.renderForm()}
        </div>
      </div>
    )
  }

  renderSpinner() {
    return (
      <div>
        <h1 className="CompetencyName">Loading skill card...</h1>
      </div>
    )
  }

  renderForm() {
    return (
      <div id="form-wrapper">
        <h1 className="competency-name">{this.state.currentSkill.CompetencyName}</h1>
      	<p id="skill-form-error" className="message" style={{display: 'none'}}></p>
  			<label id="label-proficiency" className="group-label">Proficiency</label>
      	<ul className="selectiongroup">
      		<li className={this.isProficiencySelected(1) ? 'selected' : ''} onClick={this.setProficiency(1)}>Novice</li>
      		<li className={this.isProficiencySelected(2) ? 'selected' : ''} onClick={this.setProficiency(2)}>Intermediate</li>
          <li className={this.isProficiencySelected(3) ? 'selected' : ''} onClick={this.setProficiency(3)}>Advanced</li>
          <li className={this.isProficiencySelected(4) ? 'selected' : ''} onClick={this.setProficiency(4)}>Expert</li>
      	</ul>
  			<label id="label-interest" className="group-label">Interest</label>
      	<ul className="selectiongroup">
      		<li className={this.isInterestSelected(1) ? 'selected' : ''} onClick={this.setInterest(1)}>1</li>
      		<li className={this.isInterestSelected(2) ? 'selected' : ''} onClick={this.setInterest(2)}>2</li>
          <li className={this.isInterestSelected(3) ? 'selected' : ''} onClick={this.setInterest(3)}>3</li>
          <li className={this.isInterestSelected(4) ? 'selected' : ''} onClick={this.setInterest(4)}>4</li>
      	</ul>
      	<div className="clear"></div>
        <input type="submit" value="Back" className="action" id="submit-previous" onClick={this.submitPrevious.bind(this)} />
        <input type="submit" value="Next" className="action" id="submit-next" onClick={this.submitNext.bind(this)} />
      </div>
    )
  }

  isInterestSelected(value) {
    return value == this.state.currentSkill.Interest;
  }

/**
* @todo fix this in a prettier way. Somehow could not get the ...syntax to be accepted.S
*/
  setInterest(value) {
    return () => {
      const currSkill = {};
      Object.assign(currSkill, this.state.currentSkill);
      currSkill.Interest = this.isInterestSelected(value) ? 0 : value;
      this.setState({currentSkill: currSkill});
    }
  }

  isProficiencySelected(value) {
    return value == this.state.currentSkill.Proficiency;
  }

  setProficiency(value) {
    return () => {
      const currSkill = {};
      Object.assign(currSkill, this.state.currentSkill);
      currSkill.Proficiency = this.isProficiencySelected(value) ? 0 : value;
      this.setState({currentSkill: currSkill});
    }
  }

  submitNext() {
    api.saveSkill(this.state.currentSkill, this.saveConfirmation);
    const nextSkill = this.props.stepForward(this.state.currentSkill);
    if (nextSkill) {
        this.setSkill(nextSkill);
    } else {
      api.notifyOwner(this.props.currentCompetencyCollection);
      api.redirect(`/profile/view/${this.state.currentSkill.ProfessionalID}`);
    }
  }

  submitPrevious() {
    api.saveSkill(this.state.currentSkill, this.saveConfirmation);
    const previousSkill = this.props.stepBack(this.state.currentSkill);
    if (previousSkill) {
        this.setSkill(previousSkill);
    } else {
      // Unsetting current collection will render collection holder
      this.props.unselectCurrentCollection();
    }
  }

  saveConfirmation(success) {
    // Onsuccess store the saved skill card in global store
    const msg = success ? "Skill card saved successfully" : "Failed saving skill card";
    console.log(msg);
  }

  setSkill(newSkill) {
    this.setState({currentSkill: newSkill});
  }
}
