import Skillset from './Skillset.js';
import CompetencyCollectionReducer from './CompetencyCollectionReducer.js';
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';

const store = createStore(CompetencyCollectionReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const content = (
  <Provider store={store}>
    <Skillset />
  </Provider>);

const root = document.getElementById('content');

ReactDOM.render(
  content,
  root
);
