import React from 'react'
import * as api from './api-utils.js'

export default class CompetencyCollectionHolder extends React.Component {

  render() {
    return (
      <div className="competency-collection-holder">
        {this.props.competencyCollections.map(this.renderSingleCompetencyCollectionLink.bind(this))}
        <h2
          onClick={api.enclosedRedirect('/admin/competency/')}
          key="admin">
          Create new competency collection
        </h2>
      </div>
    )
  }

  renderSingleCompetencyCollectionLink(competencyCollection) {
    return (
			  <h2
          onClick={this.props.setCurrentCompetencyCollection(competencyCollection)}
          key={competencyCollection.ID}>
          {competencyCollection.name}
        </h2>
    );
  }
}
