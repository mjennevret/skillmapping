import SkillEdit from './SkillEdit.js';
import CompetencyCollectionHolder from './CompetencyCollectionHolder.js';
import * as CompetencyCollectionActions from './CompetencyCollectionActions.js';
import * as api from './api-utils.js'
import React from 'react';
import {connect} from 'react-redux';

class Skillset extends React.Component {

  componentWillMount() {
    api.loadCompetencyCollections(memberId, this.props.setCompetencyCollections);
  }

  render() {
    if (!this.props.competencyCollections) {
      return this.renderSpinner();
    } else if (!this.props.currentCompetencyCollection) {
      api.showHeadMenu();
      return (
        <CompetencyCollectionHolder
          competencyCollections={this.props.competencyCollections}
          setCurrentCompetencyCollection={this.setCurrentWithClosedArg.bind(this)}
        />
      )
    } else {
      api.showHeadMenu(false);
      return (
        <SkillEdit
          className="skilledit"
          firstSkillCard={this.stepForward(false)}
          currentCompetencyCollection={this.props.currentCompetencyCollection}
          unselectCurrentCollection={this.props.setCurrentCompetencyCollection}
          stepForward={this.stepForward.bind(this)}
          stepBack={this.stepBack.bind(this)}
        />
      );
    }
  }

  renderSpinner() {
    return (
      <div>
        <h1 className="competency-name">Loading available competency collections...</h1>
      </div>
    );
  }

  setCurrentWithClosedArg(newCurrent) {
    return () => {
      this.props.setCurrentCompetencyCollection(newCurrent);
    }
  }

  stepBack(skillCard) {
    const {skills} = this.props.currentCompetencyCollection;
    for (let index in skills) {
      if (skills[index].ID === skillCard.ID && skills[Number(index)-1]) {
        const updatedCollection = Object.assign({}, this.props.currentCompetencyCollection);
        updatedCollection.skills[index] = skillCard;
        this.props.setCurrentCompetencyCollection(updatedCollection);
        //@todo I should keep current skill in state and have redux trigger a rerender of SkillEdit
        return skills[Number(index)-1];
      }
    }
    return null;
  }

  stepForward(skillCard = null) {
    const {currentCompetencyCollection} = this.props;
    if (!skillCard) {
      return currentCompetencyCollection.skills[0];
    }

    const {skills} = currentCompetencyCollection;
    for (let index in skills) {
      if (skills[index].ID === skillCard.ID && skills[Number(index)+1]) {
        const updatedCollection = Object.assign({}, currentCompetencyCollection);
        updatedCollection.skills[index] = skillCard;
        this.props.setCurrentCompetencyCollection(updatedCollection);
        //@todo I should keep current skill in state and have redux trigger a rerender of SkillEdit
        return skills[Number(index)+1];
      }
    }
    return null;
  }
}

const mapStateToProps = (state) => {
  const collections = state.competencyCollections;
  const currentIndex = state.currentCompetencyCollectionIndex;
  return {
    competencyCollections: collections,
    currentCompetencyCollection: collections[currentIndex]
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentCompetencyCollection: (currentCompetencyCollection) => {
      dispatch(
        CompetencyCollectionActions.setCurrentCompetencyCollection(
          currentCompetencyCollection
        )
      );
    },
    setCompetencyCollections: (collections) => {
      dispatch(
        CompetencyCollectionActions.setCompetencyCollections(
          collections
        )
      );
    }
  }
}

const SkillsetContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Skillset)

export default SkillsetContainer;
