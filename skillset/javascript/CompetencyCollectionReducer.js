const initialState = {
  competencyCollections: [],
  currentCompetencyCollectionIndex: null,
};

function setCompetencyCollections(state, action) {
  state.competencyCollections = action.payload;
  // presetCompetencyCollectionId is set by backend in the html
  if (presetCompetencyCollectionId) {
    state.currentCompetencyCollectionIndex =
      this.findCompetencyCollectionIndexById(
        state.competencyCollections,
        presetCompetencyCollectionId
      );
  }
  return Object.assign({}, state);
}

/**
 * @todo Break out into util file. It is pure and testable.
 */
function findCompetencyCollectionIndexById(competencyCollections, competencyCollectionId) {
  for(let index in competencyCollections) {
    if(competencyCollections[index].ID == competencyCollectionId ) {
      return index;
    }
  }
  return null;
}

function setCurrentCompetencyCollection(state, action) {

  let currentCompetencyCollectionIndex = null;
  if(action.payload) {
    currentCompetencyCollectionIndex = findCompetencyCollectionIndexById(
      state.competencyCollections,
      action.payload.ID
    );
    state.competencyCollections[currentCompetencyCollectionIndex] = action.payload;
  }
  return Object.assign({}, state, {currentCompetencyCollectionIndex});
}

export default function CompetencyCollectionReducer(state = initialState, action) {
  switch(action.type) {
    case 'SET_COMPETENCY_COLLECTIONS': return setCompetencyCollections(state, action);
    case 'SET_CURRENT_COMPETENCY_COLLECTION': return setCurrentCompetencyCollection(state, action);
    default: return state;
  }
}
