import $ from 'jquery';

export const loadCompetencyCollections = (memberId, callbackSetter) => {
  // The API has the memberID in the session already.
  const url = `/ProfileController/getcompetencycollections`;
  $.get(url, (data, status) => {
    if(status === 'success') {
      callbackSetter(data);
    } else {
      alert('Failure loading available competency collections');
      console.log(status, data);
    }
  });
}

export const saveSkill = (skill, callbackConfirmation) => {
  const url = `/ProfileController/saveskillcard`;
  $.ajax({
    type: "PUT",
    url: url,
    data: skill,
    dataType: 'json',
    success: (data, status) => {
      console.log(status, data);
      if(status === 'success') {
        callbackConfirmation(skill);
      } else {
        alert('Failure notifying owner of the competency collection');
        console.log(status, data);
      }
    }
  });
}

export const notifyOwner = (competencyCollection) => {
  const url = `/ProfileController/notifyowner/${competencyCollection.ID}`;
  $.get(url, (data, status) => {
    if(status === 'success') {
      console.log('Owner of the competency collection has been notified of completion');
    } else {
      console.log('FAILED notifying owner of the competency collection');
    }
  });
}

export const enclosedRedirect = (url) => {
  return () => {
    redirect(url);
  }
}

export const redirect = (url) => {
  window.location.assign(url);
}

/**
 * @todo This is dirty but not prioritized. Take care of this later.
 * @param  {Boolean} [show=true]
 * @return Void
 */
export const showHeadMenu = (show=true) => {
  if (show) {
    $('.head').show();
  } else {
    $('.head').hide();
  }
}
