/**
 * Competency Collection action generagors
 */

export function setCurrentCompetencyCollection(newCurrentCompetencyCollection=null) {
  return {type: 'SET_CURRENT_COMPETENCY_COLLECTION', payload: newCurrentCompetencyCollection};
}

export function setCompetencyCollections(collections) {
  return {type: 'SET_COMPETENCY_COLLECTIONS', payload: collections};
}
