<html>
<head>
	<% base_tag %>
	<title>$Member.Title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	$MetaTags(false)
	<% require css('skillset/css/skillful_layout.css') %>
	<link rel="shortcut icon" href="$ThemeDir/images/favicon.ico" />
</head>
<body class="Why">
<div class="main">

    <% include SkillsetHeader %>

    <div class="content">

        <h2>What is the purpose of a skillmap?</h2>
        <p>There are two situations that comes to mind for me, when I need to communicate
        my skills and ambitions. More often than notit is a tedious process that takes
        unnecessary time, and often the recieving end already has a preception that leads
        to miscommunication. Those two situations are:
            <ul>
                <li>Recruitment processes. When you talk to a lot of recruiters you want
                to make the initial process short and to the point. Also something that often is
                forgotten when ticking off those skills from a checklist, are the ambitions. Sure 
                I am very skilled at this thing I have spent 2 years doing. But am I interested in
                spending 2 more?</li>
                <li>Performance reviews, personal development talks etc. If you and your manager
                do not work very close together on a day-to-day basis and don't have these talks
                very frequently. There is a chance you will have to spend a good chunk of this
                meeting just explaining your proficiency and ambition from your own standpoint before
                any dialogue can happen.</li>
            </ul>

            My ambition with SkillMapping is to bring a tool that can be a real kickstarter
            in both those scenarios. A simple preparation that can bring you quicker to the 
            creative and interesting part of those discussions.
        </p>

        <h2>How to read the skillmap</h2>
        <p><img src="skillset/images/quadrant_example.png" style="max-width: 200px; max-height: 200px; margin-right: 10px; float: left;" />
        The skillmap is a bubble diagram with the X dimension = Proficiency, and Y dimension = Interest.
        Also the diagram is divided into 4 quadrants:<br />
        <b>Quadrant 1:</b> Low Proficiency, High Interest. This is the quadrant to work in
        if you want to maximize potential long term. Here a person is challenged, highly motivated and 
        will likely have a positive and rapid development.<br />
        <b>Quadrant 2:</b> High Proficiency, High Interest. Jackpot for everyone involved!
        Just be careful about the fat and happy-syndrome when there is not enough challenge.<br />
        <b>Quadrant 3:</b> Low Proficiency, Low Interest. Don't waste time with these skills. Nothing good comes from
        doing a bad job at something you don't want to do.<br />
        <b>Quadrant 4:</b> High Proficiency, Low Interest. These skills are a great capital.
        You may however want to think of maybe combining them with something from Quadrant 1
        to keep motivation and development up.<br />
        </p>

        <h2 style="clear: both;">So, how do I get started?</h2>
        <div class="boxed-link"><h3>#1 Sign up</h3>
            <div class="form">$RegistrationForm</div>
        </div>
        <div class="boxed-link">
            <h3>#2 Pick one of the demo competency collections to try out the app. 
                Or create your own customized collections to invite your contacts to fill out.
            </h3>
            <img src="skillset/images/competency_collections.png" />
        </div>
        <div class="boxed-link">
            <h3>#3 Click your way through the simple rating procedure.</h3>
            <img src="skillset/images/rate_skills_example.png" />
        </div>
        <div class="boxed-link">
            <h3>#4 Show the world</h3>
            <img src="skillset/images/area_example.png" />
        </div>
    </div>
</div>

<% require javascript('framework/thirdparty/jquery/jquery.js') %>
<%-- Please move: Theme javascript (below) should be moved to mysite/code/page.php  --%>
<script type="text/javascript" src="{$ThemeDir}/javascript/script.js"></script>

</body>
</html>
